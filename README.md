# initializer.d

This repo proposes an API for explicitly modeling dependencies between Rails initializers.
It is currently implemented as a proof of concept, embedded in a vanilla Rails app.

By default, Rails uses a fairly crude approach to ordering initializers, largely by running
them as they are found on the filesystem in alphanumeric order. It does have support for
modeling dependencies via `railties` initializers, but the implementation has several drawbacks
(discussed [here](#differences-to-railties-initializers)).

The aim of this POC is to solve the following problems:

* Make initializers first-class objects with a well defined API and explicit dependencies.
* Decouple declaration of initializers from their execution.
* Explicit APIs for conditional execution *(not implemented yet)*
* Frictionless coexistence with traditional initializers.

The approach taken is loosely inspired by [systemd](https://en.wikipedia.org/wiki/Systemd),
an `init.d` implementation based on lazy evaluation and explicit dependencies, as well
as dependency injection systems.

## Example

Consider initializers `A`, `B`, `C`, with `A` depending on both `B` and `C`.
This relationship can be modeled as a tree, with an outgoing edge meaning "depends on":

```text
       A
      / \
     B   C
```

Meaning, `B` and `C` should run before `A`, but between themselves can run in any order.

We would encode this as follows in `config/initializers`:

```ruby
class A
  extend Initd::Initializer

  depend_on 'B', 'C' # <= B + C defined similarly

  def run
    puts "I'm running after B and C"
  end
end
```

Then, in `application.rb` we need to hook up our initializer tree:

```ruby
class Application < Rails::Application
  ...
  Initd.mount(self)
end
```

### Run the app

This will run initializers as follows:

1. Any ordinary initializers in `config/initializers`, as specified by Rails initializer sequence rules
1. `B` and `C` in unspecified order
1. `A`

## Rules

The following rules and assumptions apply:

* Any ordinary initializer can be dropped into `config/initializers` as usual, and will
  not partake in dependency lookups. It will execute *before* any `Initd` initializer.
* An `Initd` initializer must
   1. Include the `Initd::Initializer` module
   1. Define a `run` method
* Any `Initd` initializer will see its definition loaded when ordinary initializers
  run, but will *not* execute then. All such initializers will execute together with
  `railties` initializers instead, i.e. those defined directly on the app instance.
* `Initd` initializers should not load constants as part of their class definition; references
  to application code should only happen in their `run` method, so as to postpone loading
  objects into memory until they are actually needed. This is not enforced, however.
* `Initd` initializers may have as many dependencies as needed. Dependencies will execute before
  their dependent initializers.

## Implementation

The `Initd` module uses a directed acyclic graph to maintain initializer relationships.
As initializer definitions are loaded, nodes and edges are added to represent the
dependency tree of the defined intializers. The current implementation relies on the
[dag](https://github.com/kevinrutherford/dag) gem to accomplish this.

Prior to execution, a topological sort is peformed to linearize all nodes, so that they
can be executed in the right order. The sort uses Ruby's built-in `TSort` implementation.

The root initializer installed in `application.rb` will then walk that list and call each
initializer's `run` method.

### Relevant files

Being a POC, the implementation is currently embedded into a vanilla Rails app. Here are
the files that are of interest:

* [`vendor/initializer.d`](https://gitlab.com/mttkay/initializer.d/-/tree/master/vendor/initializer.d) - the main logic, plus patches for the `dag` gem such as the ability
  to perform `tsort`s
* [`config/initializers`](https://gitlab.com/mttkay/initializer.d/-/tree/master/config/initializers) - some example initializers, organized into subfolders by hypothetical 
  application types 
* [`config/application.rb`](https://gitlab.com/mttkay/initializer.d/-/blob/master/config/application.rb) - contains the necessary `require`s and the `Initd.mount` call

## Differences to railties initializers

The closes thing Rails has to explicitly modeling initializers are `railties` initializers,
i.e. those hooked up via `Rails::Initializable` (applications and engines `include` this).
That module provides the `initializer` factory method, which can take `before` and `after` arguments
to build up rudimentary dependency chains. However, this implementation has some major limitations:

* It cannot be used to define dependencies between initializers defined in `config/initializers`, as
  those run before `application.rb` is loaded.
* It cannot define more than a single dependency for any given intializer.
* It is order sensitive: if you define a dependency on an initializer that is defined
  after the given one, an exception is raised. This makes it hard to maintain initializer chains
  defined that way as you might end up "linearizing" your definitions by hand.
