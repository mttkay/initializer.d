require_relative 'dag/dag_extensions'

module Initd
  extend self

  @dag = DAG.new

  def register(target, dependencies)
    dependent = @dag.get_or_create_vertex(target.to_s) #TODO: use strings until load path is set up

    Array(dependencies).each do |dep|
      dependency = @dag.get_or_create_vertex(dep)
      puts "#{dependent.payload} => #{dependency.payload}"
      @dag.add_edge(from: dependent, to: dependency)
    end
  end

  def run_all!
    p "Running initializers ..."

    @dag.tsort_each { |d| d.payload.constantize.new.run }
  end

  def mount(app)
    app.initializer 'init-zero' do
      Initd.run_all!
    end
  end

  module Initializer
    def depend_on(*initializers)
      Initd.register(self, initializers)
    end
  end
end
