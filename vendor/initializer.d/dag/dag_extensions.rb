class DAG
  def vertex_for(payload)
    vertices.find { |v| v.payload == payload }
  end

  def get_or_create_vertex(payload)
    vertex_for(payload) || begin
      add_vertex(payload)
    end
  end

  def tsort_each_node(&block)
    vertices.each(&block)
  end

  def tsort_each_child(vertex, &block)
    vertex.successors.each(&block)
  end

  include TSort
end
