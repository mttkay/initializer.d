module Runtime
  extend self

  def web_app?
    ENV['RUNTIME'] == 'web_app'
  end

  def background_jobs?
    ENV['RUNTIME'] == 'sidekiq'
  end
end
