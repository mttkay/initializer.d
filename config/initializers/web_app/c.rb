p "LOAD #{__FILE__}"

if Runtime.web_app?
  class C
    extend Initd::Initializer

    depend_on 'S'

    def run
      p "RUN #{self.class}"
    end
  end
end
