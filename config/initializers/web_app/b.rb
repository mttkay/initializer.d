p "LOAD #{__FILE__}"

if Runtime.web_app?
  class B
    extend Initd::Initializer

    depend_on 'C'

    def run
      p "RUN #{self.class}"
    end
  end
end
