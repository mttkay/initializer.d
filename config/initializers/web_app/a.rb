p "LOAD #{__FILE__}"

if Runtime.web_app?
  class A
    extend Initd::Initializer

    depend_on 'C', 'D'

    def run
      p "RUN #{self.class}"
    end
  end
end
