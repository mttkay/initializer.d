p "LOAD #{__FILE__}"

if Runtime.web_app?
  class D
    extend Initd::Initializer

    depend_on 'E'

    def run
      p "RUN #{self.class}"
    end
  end
end
