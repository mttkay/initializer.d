p "LOAD #{__FILE__}"

if Runtime.background_jobs?
  class Y
    extend Initd::Initializer

    depend_on 'S'

    def run
      p "RUN #{self.class}"
    end
  end
end
