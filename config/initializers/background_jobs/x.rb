p "LOAD #{__FILE__}"

if Runtime.background_jobs?
  class X
    extend Initd::Initializer

    depend_on 'S', 'Y'

    def run
      p "RUN #{self.class}"
    end
  end
end
