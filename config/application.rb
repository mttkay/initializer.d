require_relative 'boot'

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module InitializerD
  class Application < Rails::Application
    # Initialize configuration defaults for originally generated Rails version.
    config.load_defaults 6.0

    # Initd stuff
    require Rails.root.join('lib/runtime')
    require Rails.root.join('vendor/initializer.d/initializerd')

    Initd.mount(self)
  end
end
