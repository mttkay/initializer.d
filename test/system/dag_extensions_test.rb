require 'minitest/autorun'
require Rails.root.join('vendor/initializer.d/dag/dag_extensions')

describe DAG do
  before do
    @dag = DAG.new
  end

  describe 'get or create vertex' do
    it 'gets existing vertex' do
      v1 = @dag.add_vertex('v1')
      _(@dag.get_or_create_vertex('v1')).must_be_same_as v1
    end

    it 'create missing vertex' do
      _(@dag.get_or_create_vertex('v2')).must_be_instance_of DAG::Vertex
    end
  end

  describe 'topological sort' do
    #          R
    #         / \
    #        B   A
    #         \ / \
    #          C   D
    #               \
    #                E
    before do
      R = @dag.add_vertex('R')
      A = @dag.add_vertex('A')
      B = @dag.add_vertex('B')
      C = @dag.add_vertex('C')
      D = @dag.add_vertex('D')
      E = @dag.add_vertex('E')

      @dag.add_edge(from: R, to: B)
      @dag.add_edge(from: R, to: A)
      @dag.add_edge(from: B, to: C)
      @dag.add_edge(from: A, to: C)
      @dag.add_edge(from: A, to: D)
      @dag.add_edge(from: D, to: E)
    end

    it 'orders all nodes linearly' do
      _(@dag.tsort).must_equal [C, B, E, D, A, R]
    end
  end
end
